@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "date">Date</label>
            <input type="date" name="date" id="date" class="form-control" value="{{ date("Y-m-d") }}" required />
        </div>     
        <div class="form-group">
            <label for = "info">Information</label>
            <input type = "text" class="form-control" name = "info">
        </div> 
        <label for = "info">candidate name</label>
        <select class="form-control" name="id">
    @foreach($candidates as $candidate)
      <option value=" {{$candidate->id}} ">{{$candidate->name}}</option>
    @endforeach
  </select>
                    <br>          
                    <label for = "info">Interviewer name</label>
        <select class="form-control" name="user_id">
    @foreach($users as $user)

      <option value=" {{$user->id}} "> 
      <option selected="selected">{{$user->name}}</option>
      <option>{{$user->name}}</option>
    @endforeach
  </select>

                   
            <input type = "submit" name = "submit" value = "Create interview">
        </div>                       
        </form>    
@endsection