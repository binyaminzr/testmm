<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;
use App\User;
use App\Candidate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::with('candidates')->get();
        $id=Auth::id();
        // $candidates = Candidate::all();

        // $cands = $user = DB::table('candidates')->where('id',  $interviews)->first();
             
        return view('interviews.index', compact('interviews'));
    }
    public function myinterviwes()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;
        $users = User::all();       
        return view('interviews.index', compact('interviews','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.create', compact('candidates','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            $interviews = new Interview();
            $interviews->date = $request->date;
            $interviews->info = $request->info;
            $interviews->candidate_id = $request->id;
            $interviews->user_id = $request->user_id;
            $interviews->user_id = $request->id;
            // $interviews->candidate_name = $request->candidate_name;
            $interviews->save();
            return redirect('interviews');
            }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
