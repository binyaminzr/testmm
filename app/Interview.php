<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    
    public function candidates(){
        return $this->belongsTo('App\Candidate','candidate_id');
    }
    protected $fillable = [
        'date', 'info'
    ];

}
