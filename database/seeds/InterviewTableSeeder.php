<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '1.1.20',
                'info' => 'the interview was impressive',
                'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
               
            ],
            [
                'date' => '2.3.20',
                'info' => 'the interview was very bad',
                'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
              
            ],                      
            ]);            
    }
}
