<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([[
            'id' => null,
            'name' => 'before interview',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'id' => null,
            'name' => 'unusual',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'id' => null,
            'name' => 'waiting',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'id' => null,
            'name' => 'not accepted',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'id' => null,
            'name' => 'accepted',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],]
        
    );
    }
}